//Mandeep Singh 1937332
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0]= new Book ("The Lord of the Rings","J. R. R. Tolkien");
		books[1]= new ElectronicBook ("The Da Vinci Code","Dan Brown","15 Bytes");
		books[2]= new Book ("Harry Potter and the Chamber of Secrets","J. K Rowling");
		books[3]= new ElectronicBook ("The Hunger Games","Suzanne Collins","7 Bytes");
		books[4]= new ElectronicBook ("Animal Farm","George Orwell","10 Bytes");
		
		for(int i =0;i<books.length;i++) {
			System.out.println(books[i]);
		}
		
	}

}
