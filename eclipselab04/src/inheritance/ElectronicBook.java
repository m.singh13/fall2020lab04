//Mandeep Singh 1937332
package inheritance;

public class ElectronicBook extends Book{
	private String numberBytes;
	
	public ElectronicBook(String title,String author,String numberBytes) {
		super(title,author);
		this.numberBytes = numberBytes;;
	}
	
	public String toString() {
		String fromBase =super.toString();
		String fromHere = fromBase + " Bytes: " +numberBytes;
		return fromHere;
	}

}
