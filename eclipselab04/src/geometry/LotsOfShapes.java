//Mandeep Singh 1937332
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0]= new Circle (2);
		shapes[1]= new Circle(23);
		shapes[2] = new Rectangle(2,10);
		shapes[3] = new Rectangle(3.5,7.5);
		shapes[4] = new Square(3);
		
		for(int i=0;i<shapes.length;i++) {
			System.out.println("Perimeter: "+shapes[i].getPerimeter());
			System.out.println("Area: "+shapes[i].getArea());
			System.out.println("");
			
		}
		
	}

}
