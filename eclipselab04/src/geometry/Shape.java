//Mandeep Singh 1937332
package geometry;

public interface Shape {
	
	double getArea();
	double getPerimeter();

}
