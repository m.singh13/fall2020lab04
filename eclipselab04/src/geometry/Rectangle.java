//Mandeep Singh 1937332
package geometry;

public class Rectangle implements Shape {
	
	private double length,width;
	public Rectangle(double length, double width) {this.length=length;this.width=width;}
	public double getLength(){return this.length;}
	public double getWidth() {return this.width;}
	public double getArea() {return length*width;}
	public double getPerimeter() {return 2*(length+width);}

}
